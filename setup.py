from distutils.core import setup

__name__    = 'auth_jwt'
__version__ = '1.0'

requirements = []
with open('requirements.txt') as r:
    lines = r.readlines()
    for line in lines:
        if len(line) == 0:
            continue
        if line[-1] == '\n':
            line = line[:-1]
        requirements.append(line)

setup(
    name=__name__,
    version=__version__,
    packages=[__name__,],
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    long_description=open('README.txt').read(),
    install_requires=requirements,
)