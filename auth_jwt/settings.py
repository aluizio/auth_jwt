import datetime

from django.conf import settings
from rest_framework.settings import APISettings


USER_SETTINGS = getattr(settings, 'AUTH_JWT', None)


DEFAULTS = {
    'ALGORITHM': 'HS256',
    'VERIFY': True,
    'VERIFY_EXPIRATION': True,
    'LEEWAY': 0,
    'EXPIRATION_DELTA': datetime.timedelta(seconds=300),
    'AUDIENCE': None,
    'ISSUER': None,

    'ALLOW_REFRESH': False,
    'REFRESH_EXPIRATION_DELTA': datetime.timedelta(days=7),

    'AUTH_HEADER_PREFIX': 'JWT',
    'SECRET_KEY': settings.SECRET_KEY,
    'SET_COOKIE_TOKEN': False,
    'COOKIE_DOMAIN': "localhost",
    'COOKIE_PATH': "/",
    'COOKIE_KEY': "auth_jwt",
}


api_settings = APISettings(USER_SETTINGS, DEFAULTS)