from rest_framework import serializers
from datetime import timedelta
from auth_jwt.utils import *


class Serializer(serializers.Serializer):
    @property
    def object(self):
        return self.validated_data


class PasswordField(serializers.CharField):

    def __init__(self, *args, **kwargs):
        if 'style' not in kwargs:
            kwargs['style'] = {'input_type': 'password'}
        else:
            kwargs['style']['input_type'] = 'password'
        super(PasswordField, self).__init__(*args, **kwargs)



class JSONWebTokenSerializer(Serializer):
    USER_MODEL = None
    ACTIVE_USER = True
    EXPIRATION_DELTA = None
    ALLOW_REFRESH = None

    def __init__(self, *args, **kwargs):
        super(JSONWebTokenSerializer, self).__init__(*args, **kwargs)

        self.fields[self.username_field] = serializers.CharField()
        self.fields['password'] = PasswordField(write_only=True)

    @property
    def username_field(self):
        return get_username_field(self.USER_MODEL)

    def validate(self, attrs):
        credentials = {
            self.username_field: attrs.get(self.username_field),
            'password': attrs.get('password')
        }

        if all(credentials.values()):
            user = authenticate(user_model=self.USER_MODEL, **credentials)

            if user:
                if user.updateLastLogin:
                    user.updateLastLogin()
                if self.ACTIVE_USER and not user.isActiveValue:
                    msg = 'User account is disabled.'
                    raise serializers.ValidationError(msg)

                payload = jwt_payload_handler(user, expirationDelta=self.EXPIRATION_DELTA, allowRefresh=self.ALLOW_REFRESH)

                return {
                    'token': jwt_encode_handler(payload),
                    'user': user
                }
            else:
                msg = 'Unable to log in with provided credentials.'
                raise serializers.ValidationError(msg)
        else:
            msg = 'Must include "{username_field}" and "password".'
            msg = msg.format(username_field=self.username_field)
            raise serializers.ValidationError(msg)


class VerificationBaseSerializer(Serializer):
    USER_MODEL = None
    ACTIVE_USER = True
    token = serializers.CharField()

    def validate(self, attrs):
        msg = 'Please define a validate method.'
        raise NotImplementedError(msg)

    def _check_payload(self, token):
        try:
            payload = jwt_decode_handler(token)
        except jwt.ExpiredSignature:
            msg = 'Signature has expired.'
            raise serializers.ValidationError(msg)
        except jwt.DecodeError:
            msg = 'Error decoding signature.'
            raise serializers.ValidationError(msg)

        return payload

    def _check_user(self, payload):
        user_id = get_id_from_payload(payload)

        if not user_id:
            msg = 'Invalid payload.'
            raise serializers.ValidationError(msg)

        # Make sure user exists
        try:
            user = self.USER_MODEL.objects.get(id=user_id)
        except self.USER_MODEL.DoesNotExist:
            msg = "User doesn't exist."
            raise serializers.ValidationError(msg)

        if self.ACTIVE_USER and not user.isActiveValue:
            msg = 'User account is disabled.'
            raise serializers.ValidationError(msg)

        return user


class VerifyJSONWebTokenSerializer(VerificationBaseSerializer):
    def validate(self, attrs):
        token = attrs['token']

        payload = self._check_payload(token=token)
        user = self._check_user(payload=payload)

        return {
            'token': token,
            'user': user
        }


class RefreshJSONWebTokenSerializer(VerificationBaseSerializer):
    def validate(self, attrs):
        token = attrs['token']

        payload = self._check_payload(token=token)
        user = self._check_user(payload=payload)
        # Get and check 'orig_iat'
        orig_iat = payload.get('orig_iat')

        if orig_iat:
            # Verify expiration
            refresh_limit = api_settings.REFRESH_EXPIRATION_DELTA

            if isinstance(refresh_limit, timedelta):
                refresh_limit = (refresh_limit.days * 24 * 3600 +
                                 refresh_limit.seconds)

            expiration_timestamp = orig_iat + int(refresh_limit)
            now_timestamp = timegm(datetime.utcnow().utctimetuple())

            if now_timestamp > expiration_timestamp:
                msg = 'Refresh has expired.'
                raise serializers.ValidationError(msg)
        else:
            msg = 'orig_iat field is required.'
            raise serializers.ValidationError(msg)

        new_payload = jwt_payload_handler(user)
        new_payload['orig_iat'] = orig_iat

        return {
            'token': jwt_encode_handler(new_payload),
            'user': user
        }

class ForgotPasswordJSONWebTokenSerializer(Serializer):
    USER_MODEL = None
    EXPIRATION_DELTA = timedelta(seconds=3600)
    ALLOW_REFRESH = False

    def __init__(self, *args, **kwargs):
        super(ForgotPasswordJSONWebTokenSerializer, self).__init__(*args, **kwargs)
        self.fields[self.username_field] = serializers.CharField()

    @property
    def username_field(self):
        return get_username_field(self.USER_MODEL)

    def validate(self, attrs):
        userId = {}
        userId[self.username_field] = attrs.get(self.username_field)
        try:
            user = self.USER_MODEL.objects.get(**userId)
            payload = jwt_payload_handler(user, expirationDelta=self.EXPIRATION_DELTA, allowRefresh=self.ALLOW_REFRESH)

            return {
                'token': jwt_encode_handler(payload),
                'user': user
            }
        except self.USER_MODEL.DoesNotExist:
            msg = 'User not found'
            raise serializers.ValidationError(msg)

class ResetPasswordSerializer(VerifyJSONWebTokenSerializer):
    password = serializers.CharField()

    def validate(self, attrs):
        token = attrs['token']

        payload = self._check_payload(token=token)
        user = self._check_user(payload=payload)
        return {
            'user': user,
            'password': attrs['password']
        }

    def create(self, validated_data):
        user = validated_data['user']
        user.set_password(validated_data['password'])
        user.save()
        return user
