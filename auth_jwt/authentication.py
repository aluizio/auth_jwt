from rest_framework.authentication import BaseAuthentication, get_authorization_header
from rest_framework import exceptions

from django.utils.encoding import smart_text

from .utils import *


class JSONWebTokenAuthentication(BaseAuthentication):
    USER_MODEL = None
    COOKIE_KEY = None

    www_authenticate_realm = 'api'

    def authenticate(self, request):
        jwt_value = self.get_jwt_value(request)
        if jwt_value is None:
            return None

        try:
            payload = jwt_decode_handler(jwt_value)
        except jwt.ExpiredSignature:
            msg = 'Signature has expired.'
            raise exceptions.AuthenticationFailed(msg)
        except jwt.DecodeError:
            msg = 'Error decoding signature.'
            raise exceptions.AuthenticationFailed(msg)
        except jwt.InvalidTokenError:
            raise exceptions.AuthenticationFailed()

        user = self.authenticate_credentials(payload)

        return (user, jwt_value)

    def authenticate_credentials(self, payload):
        User = self.USER_MODEL
        user_id = get_id_from_payload(payload)

        if not user_id:
            msg = 'Invalid payload.'
            raise exceptions.AuthenticationFailed(msg)

        try:
            user = User.objects.get(id=user_id)
        except User.DoesNotExist:
            msg = 'Invalid signature.'
            raise exceptions.AuthenticationFailed(msg)

        if not user.isActiveValue:
            msg = 'User account is disabled.'
            raise exceptions.AuthenticationFailed(msg)

        return user

    def get_jwt_value(self, request):
        auth = get_authorization_header(request).split()
        auth_header_prefix = api_settings.AUTH_HEADER_PREFIX.lower()
        if not auth:
            if api_settings.SET_COOKIE_TOKEN:
                if self.COOKIE_KEY is None:
                    self.COOKIE_KEY = api_settings.COOKIE_KEY
                return request.COOKIES.get(self.COOKIE_KEY)
            return None

        if smart_text(auth[0].lower()) != auth_header_prefix:
            return None

        if len(auth) == 1:
            msg = 'Invalid Authorization header. No credentials provided.'
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = 'Invalid Authorization header. Credentials string should not contain spaces.'
            raise exceptions.AuthenticationFailed(msg)

        return auth[1]

    def authenticate_header(self, request):
        return '{0} realm="{1}"'.format(api_settings.AUTH_HEADER_PREFIX, self.www_authenticate_realm)
