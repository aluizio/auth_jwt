import jwt
import uuid
from calendar import timegm
from datetime import datetime

from auth_jwt.settings import api_settings

def jwt_encode_handler(payload):
    key = api_settings.SECRET_KEY
    return jwt.encode(
        payload,
        key,
        api_settings.ALGORITHM
    ).decode('utf-8')


def jwt_decode_handler(token):
    options = {
        'verify_exp': api_settings.VERIFY_EXPIRATION,
    }
    # get user from token, BEFORE verification, to get user secret key
    # unverified_payload = jwt.decode(token, None, False)
    return jwt.decode(
        token,
        api_settings.SECRET_KEY,
        api_settings.VERIFY,
        options=options,
        leeway=api_settings.LEEWAY,
        audience=api_settings.AUDIENCE,
        issuer=api_settings.ISSUER,
        algorithms=[api_settings.ALGORITHM]
    )

def jwt_payload_handler(user, expirationDelta=api_settings.EXPIRATION_DELTA, allowRefresh=api_settings.ALLOW_REFRESH):
    if expirationDelta is None:
        expirationDelta = api_settings.EXPIRATION_DELTA

    if allowRefresh is None:
        allowRefresh = api_settings.ALLOW_REFRESH

    payload = {
        'exp': datetime.utcnow() + expirationDelta
    }
    if user is not None:
        payload['user_id'] = user.pk
        if isinstance(user.pk, uuid.UUID):
            payload['user_id'] = str(user.pk)

    # Include original issued at time for a brand new token,
    # to allow token refresh
    if allowRefresh:
        payload['orig_iat'] = timegm(
            datetime.utcnow().utctimetuple()
        )

    if api_settings.AUDIENCE is not None:
        payload['aud'] = api_settings.AUDIENCE

    if api_settings.ISSUER is not None:
        payload['iss'] = api_settings.ISSUER

    return payload

def get_username_field(user_model):
    try:
        username_field = user_model.USERNAME_FIELD
    except:
        username_field = 'id'

    return username_field

def get_id_from_payload(payload):
    return payload.get('user_id')

def authenticate(username=None, password=None, user_model=None, **kwargs):
    UserModel = user_model
    if username is None:
        username = kwargs.get(UserModel.USERNAME_FIELD)
    try:
        filter_by_username = {}
        filter_by_username[UserModel.USERNAME_FIELD] = username
        user = UserModel.objects.get(**filter_by_username)
    except UserModel.DoesNotExist:
        UserModel().set_password(password)
    else:
        if user.check_password(password):
            return user
