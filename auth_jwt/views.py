from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
import datetime

from auth_jwt.settings import api_settings
from django.conf import settings

class BaseAPIView(APIView):
    permission_classes = ()
    authentication_classes = ()

class BaseJSONWebTokenAPIView(BaseAPIView):

    COOKIE_KEY = None

    def get_serializer_context(self):
        return {
            'request': self.request,
            'view': self,
        }

    def get_serializer_class(self):
        assert self.serializer_class is not None, (
            "'%s' should either include a `serializer_class` attribute, "
            "or override the `get_serializer_class()` method."
            % self.__class__.__name__)
        return self.serializer_class

    def get_serializer(self, *args, **kwargs):
        serializer_class = self.get_serializer_class()
        kwargs['context'] = self.get_serializer_context()
        return serializer_class(*args, **kwargs)

    def set_cookie(self, response, token):
        max_age = api_settings.EXPIRATION_DELTA.total_seconds()
        expires = datetime.datetime.strftime(datetime.datetime.utcnow() + api_settings.EXPIRATION_DELTA, "%a, %d-%b-%Y %H:%M:%S GMT")
        secure = True
        if settings.DEBUG:
            secure = False
        if self.COOKIE_KEY is None:
            self.COOKIE_KEY = api_settings.COOKIE_KEY
        response.set_cookie(self.COOKIE_KEY, token, max_age=max_age, expires=expires, domain=api_settings.COOKIE_DOMAIN, path=api_settings.COOKIE_PATH, httponly=True, samesite=None, secure=secure)

    def get_token(self, request):
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            return serializer.object.get('token'), serializer
        return None, serializer

class JSONWebTokenAPIView(BaseJSONWebTokenAPIView):

    def post(self, request, *args, **kwargs):
        token, serializer = self.get_token(request)
        if token is not None:
            response = Response({'token': token})
            if api_settings.SET_COOKIE_TOKEN:
                self.set_cookie(response, token)
            return response
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ForgotPasswordJSONWebTokenAPIView(BaseJSONWebTokenAPIView):

    def sendEmail(self, *args, **kwargs):
        pass

    def post(self, request, *args, **kwargs):
        token, serializer = self.get_token(request)
        if token is not None:
            self.sendEmail(request=request, token=token, user=serializer.object.get('user'))
            return Response({'status':'ok'}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
