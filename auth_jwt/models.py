from django.db import models
from django.utils import timezone
from django.contrib.auth.base_user import AbstractBaseUser


class JWTUser(AbstractBaseUser):
    is_active = models.BooleanField(default=False)

    IS_ACTIVE_FIELD = 'is_active'

    class Meta:
        abstract = True

    @property
    def isActiveValue(self):
        return getattr(self, self.IS_ACTIVE_FIELD, True)

    def updateLastLogin(self):
        self.last_login = timezone.now()
        self.save()
